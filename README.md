# Demo Use-Case sensor.community

## Projektbeschreibung

In diesem Use-Case wurde die Open Data API von [sensor.community](https://sensor.community) eingebunden. Die API liefert Feinstaubdaten, die von Community-betriebenen Messstationen aufgenommen werden.

Der Use-Case wurde auf der FIWARE-basierten [Urban Dataspace Platform](https://gitlab.com/urban-dataspace-platform/core-platform) entwickelt und integriert.

## Installation

Für die Installation und Konfiguration dieses Use-Cases wird auf das [deployment repository](https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/deployment) verwiesen

## Funktionsweise

Der NodeRED-Flow ruft die Daten des konfigurierten Sensors per HTTP-Request von der API ab. Diese Daten werden in das NGSI-kompatible [AirQualityObserved Smart-Data-Model](https://github.com/smart-data-models/dataModel.Environment/blob/master/AirQualityObserved/README.md) umgewandelt und an die Datenplattform gesendet. 


## Kontaktinformationen

Dieses Projekt wurde von der [Hypertegrity AG](https://www.hypertegrity.de/) entwickelt.

## Link zum Original-Repository

https://gitlab.com/urban-dataspace-platform/use_cases/demo_usecases/air_quality_ldinfo